<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

public function __construct()
{
	parent::__construct();
	$this->load->model('M_penjualan');
}
	public function index()
	{
		$data["barang"] = $this->M_penjualan->get_all_barang();
					$data["pembelian"]= $this->M_penjualan->get_all_pembelian();
					$data["penjualan"]=$this->M_penjualan->get_all_penjualan();
				//	$data["total"]=$this->M_penjualan->total_penjualan();
					$this->load->view('penjualan',$data);
	}
}
