<!DOCTYPE html>
<html>
<head>
	<title>Penjualan barang</title>
</head>
<body>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<div class="container">
			
		  <ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#barang">Tabel Barang</a></li>
		    <li><a data-toggle="tab" href="#penjualan">Tabel Penjualan</a></li>
		    <li><a data-toggle="tab" href="#pembelian">Tabel Pembelian</a></li>

		  </ul>

			<div class="tab-content">
    <div id="barang" class="tab-pane fade in active">
		  <table class="table table-striped">
				<h3>Data Barang </h3>
				<thead>
					<th>No</th>
					<th>nama barang</th>
					<th>stok barang</th>
				</thead>

				<?php $i=1; foreach($barang as $brg) {?>
					<tbody>
						<td><?php echo $i?></td>
						<td><?php echo $brg->nama_barang?></td>
						<td><?php echo $brg->stok_barang?></td>
					</tbody>
				<?php $i++; } ?>
			</table>

    </div>
    <div id="penjualan" class="tab-pane fade">
				  <table class="table table-striped">
				<h3>Data Penjualan Barang </h3>
				<thead>
					<th>No</th>
					<th>nama barang</th>
					<th>jumlah penjualan</th>
				</thead>
				<?php $i=1; foreach($penjualan as $pjl) {?>
					<tbody>
						<td><?php echo $i?></td>
						<td><?php echo $pjl->nama_barang?></td>
						<td><?php echo $pjl->jml_penjualan?></td>
					</tbody>
				<?php $i++; } ?>

					</table>
    </div>
    <div id="pembelian" class="tab-pane fade">
				  <table class="table table-striped">
				<h3>Data Pembelian Barang </h3>
				<thead>
					<th>No</th>
					<th>nama barang</th>
					<th>jumlah pembelian</th>
				</thead>

				<?php $i=1; foreach($pembelian as $pmb) {?>
					<tbody>
						<td><?php echo $i?></td>
						<td><?php echo $pmb->nama_barang?></td>
						<td><?php echo $pmb->jml_pembelian?></td>
					</tbody>
				<?php $i++; } ?>
			</table>


    </div>
  </div>


	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<!------ Include the above in your HEAD tag ---------->

	<br><br><div class="container">
		<div class="row">
	  <div class="col-lg-2">
	    <div class="input-group">
	      <span class="input-group-btn">
	        <button class="btn btn-default" type="button" id="btn-plus"><span class="glyphicon glyphicon-minus"></span></button>
	      </span>
	      <input type="text" class="form-control" name="qty" id="qty" value="1">
	      <span class="input-group-btn">
	        <button class="btn btn-default" type="button" id="btn-minus"><span class="glyphicon glyphicon-plus"></span></button>
	      </span>
	    </div><!-- /input-group -->
	  </div><!-- /.col-lg-6 -->
	</div><!-- /.row -->
	</div>


	<!-- <tr>
	<?php foreach($total as $a) ?>
			<td colspan="2">Total</td>
			<td><b><?php echo $a->total?></b></td>
		</tr> -->






</body>
</html>
